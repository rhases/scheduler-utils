var schedulerInfos = {};

if (typeof require !== 'undefined') {
	var _ = require('lodash');
}

schedulerInfos.weekdays = [
	{ cod: "dom", label: "Domingo" },
	{ cod: "seg", label: "Segunda-feira" },
	{ cod: "ter", label: "Terça-feira" },
	{ cod: "qua", label: "Quarta-feira" },
	{ cod: "qui", label: "Quinta-feira" },
	{ cod: "sex", label: "Sexta-feira" },
	{ cod: "sab", label: "Sábado" },
];

schedulerInfos.periods = [
	{ cod: "inicio-da-manha", label: "Início da manhã (7h-10h)" },
	{ cod: "fim-da-manha", label: "Fim da manhã (10h-12h)" },
	{ cod: "horario-de-almoco", label: "Horário do almoço (12h-14h)" },
	{ cod: "inicio-da-tarde", label: "Início da tarde (14h-16h)" },
	{ cod: "fim-da-tarde", label: "Fim da tarde (16h-18h)" },
	{ cod: "inicio-da-noite", label: "Início da noite (18h-20h)" },
];

schedulerInfos.getWeekdayByLabel = function(label) {
	if (!schedulerInfos.weekdays || !label)
		return;

	var index = _.findIndex(schedulerInfos.weekdays, function(s) { return s.label.toLowerCase() === label.toLowerCase(); })

	if (index >= 0)
		return schedulerInfos.weekdays[index];
	return undefined;
}

schedulerInfos.getWeekdayByCod = function(cod) {
	if (!schedulerInfos.weekdays || !cod)
		return;

	var index = _.findIndex(schedulerInfos.weekdays, function(s) { return s.cod.toLowerCase() === cod.toLowerCase(); })

	if (index >= 0)
		return schedulerInfos.weekdays[index];
	return undefined;
}

schedulerInfos.getPeriodByLabel = function(label) {
	if (!schedulerInfos.periods || !label)
		return;

	var index = _.findIndex(schedulerInfos.periods, function(s) { return s.label.toLowerCase() === label.toLowerCase(); })

	if (index >= 0)
		return schedulerInfos.periods[index];
	return undefined;
}

schedulerInfos.getPeriodByCod = function(cod) {
	if (!schedulerInfos.periods || !cod)
		return;

	var index = _.findIndex(schedulerInfos.periods, function(s) { return s.cod.toLowerCase() === cod.toLowerCase(); })

	if (index >= 0)
		return schedulerInfos.periods[index];
	return undefined;
}

schedulerInfos.allWeekdayCodes = function() {
	if (!schedulerInfos.weekdays)
		return;

	var codes = [];
	schedulerInfos.weekdays.forEach(function(s) { codes.push(s.cod); });
	return codes;
}

schedulerInfos.allPeriodCodes = function() {
	if (!schedulerInfos.periods)
		return;

	var codes = [];
	schedulerInfos.periods.forEach(function(s) { codes.push(s.cod); });
	return codes;
}

schedulerInfos.allBusinessWeekdays = function() {
	if (!schedulerInfos.weekdays)
		return;
	return _.slice(_.clone(schedulerInfos.weekdays), 1);
}

if (typeof module !== 'undefined') {
	module.exports = schedulerInfos;
}
